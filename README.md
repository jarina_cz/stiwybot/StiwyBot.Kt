# StiwyBot.Kt

A Simple Discord Bot written in Kotlin using a JDA library.

## Installation

```powershell
git clone https://gitlab.com/jarina_cz/stiwybot/StiwyBot.Kt.git StiwyBot
cd StiwyBot
```

Get token from Discord Developers page and then create environment variable

```powershell
# Linux
export DISCORD_BOT_TOKEN_STIWY_BOT="your_token"
# Windows PowerShell
$Env:DISCORD_BOT_TOKEN_STIWY_BOT="your_token"
```

You can update configuration in
```
StiwyBot\src\main\resources\app.properties
```

Open Project in Jetbrains IDEA

## Tests
- Not yet implemented

## LICENSE

MIT
