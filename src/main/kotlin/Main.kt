import net.dv8tion.jda.core.AccountType
import net.dv8tion.jda.core.JDABuilder
import com.natpryce.konfig.*
import events.*

val config: Configuration = EnvironmentVariables() overriding
        ConfigurationProperties.fromResource("app.properties")

@Throws(Exception::class)
fun main(arguments: Array<String>) {
    val discordBotToken = Key("discord-bot-token", stringType)
    val token = System.getenv("DISCORD_BOT_TOKEN_STIWY_BOT") ?: config[discordBotToken]

    val jda = JDABuilder(AccountType.BOT)
            .setToken(token)
            .addEventListener(ClientEvents())
            .build()
    jda.addEventListener(ChannelEvents())
    jda.addEventListener(RoleEvents())
    jda.addEventListener(GuildEvents())
    jda.addEventListener(MessageEvents())
}