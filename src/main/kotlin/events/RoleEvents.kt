package events

import net.dv8tion.jda.core.events.role.RoleCreateEvent
import net.dv8tion.jda.core.events.role.RoleDeleteEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter

class RoleEvents : ListenerAdapter() {
    override fun onRoleCreate(event: RoleCreateEvent?) {
        println("A role ${event!!.role.name} has been created")
    }

    override fun onRoleDelete(event: RoleDeleteEvent?) {
        println("A role ${event!!.role.name} has been deleted")
    }
}