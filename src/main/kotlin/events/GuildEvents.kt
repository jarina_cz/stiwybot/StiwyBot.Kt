package events

import net.dv8tion.jda.core.events.guild.GuildBanEvent
import net.dv8tion.jda.core.events.guild.GuildUnbanEvent
import net.dv8tion.jda.core.events.guild.member.GuildMemberJoinEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter

class GuildEvents : ListenerAdapter() {
    override fun onGuildMemberJoin(event: GuildMemberJoinEvent?) {
        event!!.guild.defaultChannel!!.sendMessage("Please welcome ${event.member.nickname} to the server").queue()
        println("User ${event.member.nickname} joined ${event.guild.name}")
    }

    override fun onGuildBan(event: GuildBanEvent?) {
        event!!.guild.defaultChannel!!.sendMessage("${event.user.name} was just banned").queue()
    }

    override fun onGuildUnban(event: GuildUnbanEvent?) {
        event!!.guild.defaultChannel!!.sendMessage("${event.user.name} was just unbanned").queue()
    }
}